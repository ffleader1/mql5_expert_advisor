//+------------------------------------------------------------------+
//|                                                  data_reader.mq5 |
//|                                  Copyright 2021, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"
#include <Trade\Trade.mqh>

#define AMOUNT 5
#define MAX_NEURON 132
#define EXPERT_MAGIC 123459
#define MAX_OPEN 5
#define TICK_COOL_DOWN 15

int tick_cool_down;
int amount=AMOUNT;

#define TIMEFRAME_1 14;
#define TIMEFRAME_2 24;

int timeframe1= TIMEFRAME_1; int timeframe2=TIMEFRAME_2;

ENUM_APPLIED_PRICE   applied_price=PRICE_TYPICAL;
ENUM_APPLIED_VOLUME  applied_volume=VOLUME_TICK;

int            iRSI_handle_m2_tf1, iRSI_handle_m2_tf2,iRSI_handle_m3_tf1, iRSI_handle_m3_tf2,iRSI_handle_m5_tf1, iRSI_handle_m5_tf2, iRSI_handle_m10_tf1, iRSI_handle_m10_tf2;
double         iRSI_buf_m2_tf1[], iRSI_buf_m2_tf2[], iRSI_buf_m3_tf1[], iRSI_buf_m3_tf2[],iRSI_buf_m5_tf1[], iRSI_buf_m5_tf2[],iRSI_buf_m10_tf1[], iRSI_buf_m10_tf2[];

int            iStochastic_handle_m2,iStochastic_handle_m3,iStochastic_handle_m5,iStochastic_handle_m10;
double         StochasticBuffer_m2[],StochasticBuffer_m3[],StochasticBuffer_m5[],StochasticBuffer_m10[];
double         StochasticSignalBuffer_m2[],StochasticSignalBuffer_m3[],StochasticSignalBuffer_m5[],StochasticSignalBuffer_m10[];

int            iStochastic_handle_m2_tf2,iStochastic_handle_m3_tf2,iStochastic_handle_m5_tf2,iStochastic_handle_m10_tf2;
double         StochasticBuffer_m2_tf2[],StochasticBuffer_m3_tf2[],StochasticBuffer_m5_tf2[],StochasticBuffer_m10_tf2[];
double         StochasticSignalBuffer_m2_tf2[],StochasticSignalBuffer_m3_tf2[],StochasticSignalBuffer_m5_tf2[],StochasticSignalBuffer_m10_tf2[];


int            iMFI_handle_m2_tf1, iMFI_handle_m2_tf2,iMFI_handle_m3_tf1, iMFI_handle_m3_tf2,iMFI_handle_m5_tf1, iMFI_handle_m5_tf2, iMFI_handle_m10_tf1, iMFI_handle_m10_tf2;
double         iMFI_buf_m2_tf1[], iMFI_buf_m2_tf2[], iMFI_buf_m3_tf1[], iMFI_buf_m3_tf2[],iMFI_buf_m5_tf1[], iMFI_buf_m5_tf2[],iMFI_buf_m10_tf1[], iMFI_buf_m10_tf2[];

int            iUO_handle_m2_tf1, iUO_handle_m2_tf2, iUO_handle_m3_tf1, iUO_handle_m3_tf2, iUO_handle_m5_tf1, iUO_handle_m5_tf2, iUO_handle_m10_tf1, iUO_handle_m10_tf2;
double         iUO_buf_m2_tf1[], iUO_buf_m2_tf2[], iUO_buf_m3_tf1[], iUO_buf_m3_tf2[], iUO_buf_m5_tf1[], iUO_buf_m5_tf2[], iUO_buf_m10_tf1[], iUO_buf_m10_tf2[];

int            iWPR_handle_m2_tf1, iWPR_handle_m2_tf2, iWPR_handle_m3_tf1, iWPR_handle_m3_tf2, iWPR_handle_m5_tf1, iWPR_handle_m5_tf2, iWPR_handle_m10_tf1, iWPR_handle_m10_tf2;
double         iWPR_buf_m2_tf1[], iWPR_buf_m2_tf2[], iWPR_buf_m3_tf1[], iWPR_buf_m3_tf2[], iWPR_buf_m5_tf1[], iWPR_buf_m5_tf2[], iWPR_buf_m10_tf1[], iWPR_buf_m10_tf2[];

double         low_buf[], high_buf[];
bool shutdown = false;
int shutdown_count=10;

int            iGolden_handle;
double         iGolden_buf[];

int               iFib_handle;
double            iFib_buf[];

double            High_buf[];
double            Low_buf[];
double            Open_buf[];
double            Close_buf[];

int debug_file_handler=FileOpen("debug.csv",FILE_WRITE|FILE_ANSI|FILE_CSV);

string act_func_1 [],act_func_2 [],act_func_3 [],act_func_4 [],act_func_5 [];
double network_1 [][MAX_NEURON][MAX_NEURON],network_2 [][MAX_NEURON][MAX_NEURON],network_3 [][MAX_NEURON][MAX_NEURON],network_4 [][MAX_NEURON][MAX_NEURON],network_5 [][MAX_NEURON][MAX_NEURON];

datetime pausetime=TimeCurrent();
datetime m_prev_bars                = 0;
int count_buy=0,count_sell=0;

CTrade                  m_trade;
CPositionInfo           m_position;

struct Trader
  {
   int  trend;
   double  score_buff[];
//   double  temporary_score;
   double saved_sum;
//   int candle_count;
   double bid;
   double ask;
   //short  reserved2;
  };

bool enable_auto_stop = true;
  
   Trader trade;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   iRSI_handle_m2_tf1=iRSI(Symbol(),PERIOD_M2,timeframe1,applied_price);
   iRSI_handle_m3_tf1=iRSI(Symbol(),PERIOD_M3,timeframe1,applied_price);
   iRSI_handle_m5_tf1=iRSI(Symbol(),PERIOD_M5,timeframe1,applied_price);
   iRSI_handle_m10_tf1=iRSI(Symbol(),PERIOD_M10,timeframe1,applied_price);

   iRSI_handle_m2_tf2=iRSI(Symbol(),PERIOD_M2,timeframe2,applied_price);
   iRSI_handle_m3_tf2=iRSI(Symbol(),PERIOD_M3,timeframe2,applied_price);
   iRSI_handle_m5_tf2=iRSI(Symbol(),PERIOD_M5,timeframe2,applied_price);
   iRSI_handle_m10_tf2=iRSI(Symbol(),PERIOD_M10,timeframe2,applied_price);

   iStochastic_handle_m2=iStochastic(Symbol(),PERIOD_M2,5,3,3,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m3=iStochastic(Symbol(),PERIOD_M3,5,3,3,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m5=iStochastic(Symbol(),PERIOD_M5,5,3,3,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m10=iStochastic(Symbol(),PERIOD_M10,5,3,3,MODE_SMA,STO_LOWHIGH);

   iStochastic_handle_m2_tf2=iStochastic(Symbol(),PERIOD_M2,13,8,8,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m3_tf2=iStochastic(Symbol(),PERIOD_M3,13,8,8,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m5_tf2=iStochastic(Symbol(),PERIOD_M5,13,8,8,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m10_tf2=iStochastic(Symbol(),PERIOD_M10,13,8,8,MODE_SMA,STO_LOWHIGH);

   iMFI_handle_m2_tf1=iMFI(Symbol(),PERIOD_M2,timeframe1,applied_volume);
   iMFI_handle_m3_tf1=iMFI(Symbol(),PERIOD_M3,timeframe1,applied_volume);
   iMFI_handle_m5_tf1=iMFI(Symbol(),PERIOD_M5,timeframe1,applied_volume);
   iMFI_handle_m10_tf1=iMFI(Symbol(),PERIOD_M10,timeframe1,applied_volume);

   iMFI_handle_m2_tf2=iMFI(Symbol(),PERIOD_M2,timeframe2,applied_volume);
   iMFI_handle_m3_tf2=iMFI(Symbol(),PERIOD_M3,timeframe2,applied_volume);
   iMFI_handle_m5_tf2=iMFI(Symbol(),PERIOD_M5,timeframe2,applied_volume);
   iMFI_handle_m10_tf2=iMFI(Symbol(),PERIOD_M10,timeframe2,applied_volume);

   iUO_handle_m2_tf1=iCustom(Symbol(),PERIOD_M2,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   iUO_handle_m3_tf1=iCustom(Symbol(),PERIOD_M3,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   iUO_handle_m5_tf1=iCustom(Symbol(),PERIOD_M5,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   iUO_handle_m10_tf1=iCustom(Symbol(),PERIOD_M10,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);

   iUO_handle_m2_tf2=iCustom(Symbol(),PERIOD_M2,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   iUO_handle_m3_tf2=iCustom(Symbol(),PERIOD_M3,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   iUO_handle_m5_tf2=iCustom(Symbol(),PERIOD_M5,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   iUO_handle_m10_tf2=iCustom(Symbol(),PERIOD_M10,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);

   iWPR_handle_m2_tf1=iCustom(Symbol(),PERIOD_M2,"Examples\\WPR",timeframe1);
   iWPR_handle_m3_tf1=iCustom(Symbol(),PERIOD_M3,"Examples\\WPR",timeframe1);
   iWPR_handle_m5_tf1=iCustom(Symbol(),PERIOD_M5,"Examples\\WPR",timeframe1);
   iWPR_handle_m10_tf1=iCustom(Symbol(),PERIOD_M10,"Examples\\WPR",timeframe1);

   iWPR_handle_m2_tf2=iCustom(Symbol(),PERIOD_M2,"Examples\\WPR",timeframe2);
   iWPR_handle_m3_tf2=iCustom(Symbol(),PERIOD_M3,"Examples\\WPR",timeframe2);
   iWPR_handle_m5_tf2=iCustom(Symbol(),PERIOD_M5,"Examples\\WPR",timeframe2);
   iWPR_handle_m10_tf2=iCustom(Symbol(),PERIOD_M10,"Examples\\WPR",timeframe2);

   iGolden_handle=iCustom(Symbol(),PERIOD_M5,"Custom\\Golden_Ratio",timeframe1);

   ArraySetAsSeries(iRSI_buf_m2_tf1,true);
   ArraySetAsSeries(iRSI_buf_m3_tf1,true);
   ArraySetAsSeries(iRSI_buf_m5_tf1,true);
   ArraySetAsSeries(iRSI_buf_m10_tf1,true);

   ArraySetAsSeries(iRSI_buf_m2_tf2,true);
   ArraySetAsSeries(iRSI_buf_m3_tf2,true);
   ArraySetAsSeries(iRSI_buf_m5_tf2,true);
   ArraySetAsSeries(iRSI_buf_m10_tf2,true);

   load_network("RSI_model.txt",1);
   load_network("Stoch_model.txt",2);
   load_network("MFI_model.txt",3);
   load_network("UO_model.txt",4);
   load_network("WPR_model.txt",5);

   double inputs[]={21, 4, 19, 96, 9, 11, 19, 99, 21, 4, 19, 96, 9, 11, 19, 99};
   double outputs[];



   propagate(inputs,outputs,1);

   Print("Output len is: ", ArraySize(outputs)," | Network Size: ", ArraySize(network_1));
   ArrayPrint(outputs);
   //shutdown=true;

   iFib_handle=iCustom(Symbol(),PERIOD_CURRENT,"Custom\\Fibonacci_Point",14);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
    ArrayFree(iFib_buf);
    Print("BUY: ", count_buy);
    Print("SELL: ", count_sell);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick(){

   if (shutdown || shutdown_count<=0){
      return;
   }

   datetime time_0=iTime(Symbol(),Period(),0);
   if(time_0==m_prev_bars){
      if (trade.trend!=0 && enable_auto_stop){
          int err3=CopyHigh(Symbol(),PERIOD_CURRENT,0,amount,High_buf);
          int err4=CopyLow(Symbol(),PERIOD_CURRENT,0,amount,Low_buf);
          err3=CopyOpen(Symbol(),PERIOD_CURRENT,0,amount,Open_buf);
          err4=CopyClose(Symbol(),PERIOD_CURRENT,0,amount,Close_buf);

          double temp_score=0;

          if (High_buf[0]!=Low_buf[0]){
            temp_score=(Close_buf[0]-Open_buf[0])*MathSqrt(MathAbs(Close_buf[0]-Open_buf[0])/(High_buf[0]-Low_buf[0]));
          }
          double temp_score_buff[];
          int array_size=ArraySize(trade.score_buff)+1;
          ArrayResize(temp_score_buff,array_size);
          ArrayCopy(temp_score_buff,trade.score_buff);
          temp_score_buff[array_size-1]=temp_score;

          double cvp=cvp_calculator(temp_score_buff);

          double cvp_abs=MathAbs(cvp);
          double current_sum_abs=cvp_abs*(-0.1323*array_size+2.0584);
          bool close_trade=false;
          if (current_sum_abs<=0){
              if (trade.trend==1){
                if (SYMBOL_BID>=trade.ask){
                    close_trade=true;
                    Print("Stop 1");
                }
              }else {
                if (SYMBOL_ASK>=trade.bid){
                    close_trade=true;
                    Print("Stop 2");

                }
              }
          }else if (cvp*trade.saved_sum>0){
              double saved_sum_abs=MathAbs(trade.saved_sum);
              if (saved_sum_abs>current_sum_abs){
                    close_trade=true;
                    Print("Stop 3");
              }
          }
          if (array_size>21){
            close_trade=true;
            Print("Stop 4 - FORCE");
          }
          if (close_trade){
            CloseAllPositions();
            trade.trend=0;
            ArrayFree(trade.score_buff);
            ArrayResize(trade.score_buff,0);
          }
      }
      return;
   }
   m_prev_bars=time_0;

//---
   int err1;
   err1=CopyBuffer(iRSI_handle_m2_tf1,0,1,1,iRSI_buf_m2_tf1);
   err1=CopyBuffer(iRSI_handle_m3_tf1,0,1,1,iRSI_buf_m3_tf1);
   err1=CopyBuffer(iRSI_handle_m5_tf1,0,1,1,iRSI_buf_m5_tf1);
   err1=CopyBuffer(iRSI_handle_m10_tf1,0,1,1,iRSI_buf_m10_tf1);

   err1=CopyBuffer(iRSI_handle_m2_tf2,0,1,1,iRSI_buf_m2_tf2);
   err1=CopyBuffer(iRSI_handle_m3_tf2,0,1,1,iRSI_buf_m3_tf2);
   err1=CopyBuffer(iRSI_handle_m5_tf2,0,1,1,iRSI_buf_m5_tf2);
   err1=CopyBuffer(iRSI_handle_m10_tf2,0,1,1,iRSI_buf_m10_tf2);

   err1=CopyBuffer(iStochastic_handle_m2,0,1,1,StochasticBuffer_m2);
   err1=CopyBuffer(iStochastic_handle_m3,0,1,1,StochasticBuffer_m3);
   err1=CopyBuffer(iStochastic_handle_m5,0,1,1,StochasticBuffer_m5);
   err1=CopyBuffer(iStochastic_handle_m10,0,1,1,StochasticBuffer_m10);

   err1=CopyBuffer(iStochastic_handle_m2,1,1,1,StochasticSignalBuffer_m2);
   err1=CopyBuffer(iStochastic_handle_m3,1,1,1,StochasticSignalBuffer_m3);
   err1=CopyBuffer(iStochastic_handle_m5,1,1,1,StochasticSignalBuffer_m5);
   err1=CopyBuffer(iStochastic_handle_m10,1,1,1,StochasticSignalBuffer_m10);

   err1=CopyBuffer(iStochastic_handle_m2_tf2,0,1,1,StochasticBuffer_m2_tf2);
   err1=CopyBuffer(iStochastic_handle_m3,0,1,1,StochasticBuffer_m3_tf2);
   err1=CopyBuffer(iStochastic_handle_m5,0,1,1,StochasticBuffer_m5_tf2);
   err1=CopyBuffer(iStochastic_handle_m10,0,1,1,StochasticBuffer_m10_tf2);

   err1=CopyBuffer(iStochastic_handle_m2_tf2,1,1,1,StochasticSignalBuffer_m2_tf2);
   err1=CopyBuffer(iStochastic_handle_m3_tf2,1,1,1,StochasticSignalBuffer_m3_tf2);
   err1=CopyBuffer(iStochastic_handle_m5_tf2,1,1,1,StochasticSignalBuffer_m5_tf2);
   err1=CopyBuffer(iStochastic_handle_m10_tf2,1,1,1,StochasticSignalBuffer_m10_tf2);

   err1=CopyBuffer(iMFI_handle_m2_tf1,0,1,1,iMFI_buf_m2_tf1);
   err1=CopyBuffer(iMFI_handle_m3_tf1,0,1,1,iMFI_buf_m3_tf1);
   err1=CopyBuffer(iMFI_handle_m5_tf1,0,1,1,iMFI_buf_m5_tf1);
   err1=CopyBuffer(iMFI_handle_m10_tf1,0,1,1,iMFI_buf_m10_tf1);

   err1=CopyBuffer(iMFI_handle_m2_tf2,0,1,1,iMFI_buf_m2_tf2);
   err1=CopyBuffer(iMFI_handle_m3_tf2,0,1,1,iMFI_buf_m3_tf2);
   err1=CopyBuffer(iMFI_handle_m5_tf2,0,1,1,iMFI_buf_m5_tf2);
   err1=CopyBuffer(iMFI_handle_m10_tf2,0,1,1,iMFI_buf_m10_tf2);

   err1=CopyBuffer(iUO_handle_m2_tf1,0,1,1,iUO_buf_m2_tf1);
   err1=CopyBuffer(iUO_handle_m3_tf1,0,1,1,iUO_buf_m3_tf1);
   err1=CopyBuffer(iUO_handle_m5_tf1,0,1,1,iUO_buf_m5_tf1);
   err1=CopyBuffer(iUO_handle_m10_tf1,0,1,1,iUO_buf_m10_tf1);

   err1=CopyBuffer(iUO_handle_m2_tf2,0,1,1,iUO_buf_m2_tf2);
   err1=CopyBuffer(iUO_handle_m3_tf2,0,1,1,iUO_buf_m3_tf2);
   err1=CopyBuffer(iUO_handle_m5_tf2,0,1,1,iUO_buf_m5_tf2);
   err1=CopyBuffer(iUO_handle_m10_tf2,0,1,1,iUO_buf_m10_tf2);

   err1=CopyBuffer(iWPR_handle_m2_tf1,0,1,1,iWPR_buf_m2_tf1);
   err1=CopyBuffer(iWPR_handle_m3_tf1,0,1,1,iWPR_buf_m3_tf1);
   err1=CopyBuffer(iWPR_handle_m5_tf1,0,1,1,iWPR_buf_m5_tf1);
   err1=CopyBuffer(iWPR_handle_m10_tf1,0,1,1,iWPR_buf_m10_tf1);

   err1=CopyBuffer(iWPR_handle_m2_tf2,0,1,1,iWPR_buf_m2_tf2);
   err1=CopyBuffer(iWPR_handle_m3_tf2,0,1,1,iWPR_buf_m3_tf2);
   err1=CopyBuffer(iWPR_handle_m5_tf2,0,1,1,iWPR_buf_m5_tf2);
   err1=CopyBuffer(iWPR_handle_m10_tf2,0,1,1,iWPR_buf_m10_tf2);


   int err2;
   err2=CopyBuffer(iFib_handle,0,0,1,iFib_buf);

   int err3=CopyHigh(Symbol(),PERIOD_CURRENT,1,amount,High_buf);
   int err4=CopyLow(Symbol(),PERIOD_CURRENT,1,amount,Low_buf);
   err3=CopyOpen(Symbol(),PERIOD_CURRENT,1,amount,Open_buf);
   err4=CopyClose(Symbol(),PERIOD_CURRENT,1,amount,Close_buf);

   //-----------------------------------------------
   if (trade.trend!=0 && enable_auto_stop){
     int array_size=ArraySize(trade.score_buff)+1;
     ArrayResize(trade.score_buff,array_size);
     if (High_buf[0]==Low_buf[0]){
        trade.score_buff[array_size-1]=0;
     }else {
        trade.score_buff[array_size-1]=(Close_buf[0]-Open_buf[0])*MathSqrt(MathAbs(Close_buf[0]-Open_buf[0])/(High_buf[0]-Low_buf[0]));
     }
   }

   //-----------------------------------------------

   if(trade.trend==0|| !enable_auto_stop){

       int condition=check_condition(Open_buf,Close_buf,High_buf,Low_buf) ;

       double inputs_1[]={iRSI_buf_m2_tf1[0],iRSI_buf_m3_tf1[0],iRSI_buf_m5_tf1[0],iRSI_buf_m10_tf1[0],
                         iRSI_buf_m2_tf2[0],iRSI_buf_m3_tf2[0],iRSI_buf_m5_tf2[0],iRSI_buf_m10_tf2[0],
//                         iUO_buf_m2_tf1[0],iUO_buf_m3_tf1[0],iUO_buf_m5_tf1[0],iUO_buf_m10_tf1[0],
//                         iUO_buf_m2_tf2[0],iUO_buf_m3_tf2[0],iUO_buf_m5_tf2[0],iUO_buf_m10_tf2[0]
                        StochasticBuffer_m2[0],StochasticBuffer_m3[0],
                        StochasticBuffer_m5[0],StochasticBuffer_m10[0],
                         StochasticSignalBuffer_m2[0],StochasticSignalBuffer_m3[0],
                         StochasticSignalBuffer_m5[0],StochasticSignalBuffer_m10[0],
                         StochasticBuffer_m2_tf2[0],StochasticBuffer_m3_tf2[0],
                         StochasticBuffer_m5_tf2[0],StochasticBuffer_m10_tf2[0],
                          StochasticSignalBuffer_m2_tf2[0],StochasticSignalBuffer_m3_tf2[0],
                          StochasticSignalBuffer_m5_tf2[0],StochasticSignalBuffer_m10_tf2[0]
                                              };
       double output_1[];

       double inputs_2[]={StochasticBuffer_m2[0],StochasticBuffer_m3[0],StochasticBuffer_m5[0],StochasticBuffer_m10[0],
                         StochasticSignalBuffer_m2[0],StochasticSignalBuffer_m3[0],StochasticSignalBuffer_m5[0],StochasticSignalBuffer_m10[0]};
       double output_2[];

       double inputs_3[]={iMFI_buf_m2_tf1[0],iMFI_buf_m3_tf1[0],iMFI_buf_m5_tf1[0],iMFI_buf_m10_tf1[0],
                         iMFI_buf_m2_tf2[0],iMFI_buf_m3_tf2[0],iMFI_buf_m5_tf2[0],iMFI_buf_m10_tf2[0]};

       double output_3[];

       double inputs_4[]={iUO_buf_m2_tf1[0],iUO_buf_m3_tf1[0],iUO_buf_m5_tf1[0],iUO_buf_m10_tf1[0],
                         iUO_buf_m2_tf2[0],iUO_buf_m3_tf2[0],iUO_buf_m5_tf2[0],iUO_buf_m10_tf2[0]};

       double output_4[];

       double inputs_5[]={iWPR_buf_m2_tf1[0],iWPR_buf_m3_tf1[0],iWPR_buf_m5_tf1[0],iWPR_buf_m10_tf1[0],
                         iWPR_buf_m2_tf2[0],iWPR_buf_m3_tf2[0],iWPR_buf_m5_tf2[0],iWPR_buf_m10_tf2[0]};

       double output_5[];

       int open=-1;
       int c_buy=0, c_sell=0;
       if (condition>=0){
          string outputData="";
          for (int i=0;i<ArraySize(inputs_1);i++){
             outputData+=DoubleToString(inputs_1[i],10);
             if (i!=ArraySize(inputs_1)-1){
                outputData+=",";
             }else{
                outputData+=","+IntegerToString(condition)+"\r\n";
             }
          }
          FileWriteString(debug_file_handler,outputData);
          propagate(inputs_1, output_1,1);

         // propagate(inputs_2, output_2,2);
          //propagate(inputs_3, output_3,3);
          //propagate(inputs_4, output_4,4);
          //propagate(inputs_5, output_5,5);
          int result_array[5];
          for (int i=0;i<ArrayMaximum(result_array);i++){
             result_array[i]=-1;
          }

          result_array[0]=ArrayMaximum(output_1);
          //ArrayPrint(inputs_1);
          //ArrayPrint(output_1);
          if (result_array[0] ==0 || result_array[0]==1){
              if (result_array[0] == condition){
                //Print("Correct direction suggestion");
                 open=condition;
              }else{
                //Print("Bad direction suggestion");
              }
          }else{
                //Print("Suggest to not enter");
          }

       }


       if ( (open==0||open==1) && pausetime<=TimeCurrent() ){
             MqlTradeRequest request={};
             MqlTradeResult  result={};
             request.action   =TRADE_ACTION_DEAL;                     // type of trade operation
             request.symbol   =Symbol();                              // symbol
             request.volume   =SymbolInfoDouble(Symbol(),SYMBOL_VOLUME_MIN);
             request.type_filling = SYMBOL_FILLING_FOK|ORDER_FILLING_FOK;

             request.deviation=5;                                     // allowed deviation from the price
             request.magic    =EXPERT_MAGIC;
             if (open==1){
                request.type     =ORDER_TYPE_BUY;                        // order type
                request.price    =SymbolInfoDouble(Symbol(),SYMBOL_ASK); // price for opening
                request.tp=SymbolInfoDouble(Symbol(),SYMBOL_ASK) +0.0005;
                request.sl=SymbolInfoDouble(Symbol(),SYMBOL_BID) -0.0008;
             }else if(open==0){
                request.type     =ORDER_TYPE_SELL;                        // order type
                request.price    =SymbolInfoDouble(Symbol(),SYMBOL_BID); // price for opening
                request.tp=SymbolInfoDouble(Symbol(),SYMBOL_BID) -0.0005;
                request.sl=SymbolInfoDouble(Symbol(),SYMBOL_ASK) +0.0008;
             }
             if(!OrderSend(request,result)){
                PrintFormat("OrderSend error %d",GetLastError());     // if unable to send the request, output the error code
             }else {
                if (enable_auto_stop){
                   trade.trend=2*open-1;
                   trade.bid=SYMBOL_BID;
                   trade.ask=SYMBOL_ASK;
                }
             }

             PrintFormat("Order Send Succesfully retcode=%u  deal=%I64u  order=%I64u",result.retcode,result.deal,result.order);
             pausetime=TimeCurrent()+GetMinute()*60*5;

       }
   }
}
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester()
  {
//---
   double ret=0.0;
//---

//---
   return(ret);
  }
//+------------------------------------------------------------------+

void propagate(double const &original_inputs[],double &result[], int network_no){

   string act_func [];
   double network [][MAX_NEURON][MAX_NEURON];

   if (network_no==1) {
      ArrayResize(act_func,ArraySize(act_func_1));
      ArrayResize(network,ArraySize(act_func_1));
   }else if (network_no==2){
      ArrayResize(act_func,ArraySize(act_func_2));
      ArrayResize(network,ArraySize(act_func_2));
   }else if (network_no==3){
      ArrayResize(act_func,ArraySize(act_func_3));
      ArrayResize(network,ArraySize(act_func_3));
   }else if (network_no==4){
      ArrayResize(act_func,ArraySize(act_func_4));
      ArrayResize(network,ArraySize(act_func_4));
   }else{
      ArrayResize(act_func,ArraySize(act_func_5));
      ArrayResize(network,ArraySize(act_func_5));
   }

   for (int i=0;i<ArraySize(act_func);i++){
         if (network_no==1) {
            act_func[i]=act_func_1[i];
         }else if (network_no==2) {
            act_func[i]=act_func_2[i];
         }else if (network_no==3) {
            act_func[i]=act_func_3[i];
         }else if (network_no==4) {
            act_func[i]=act_func_4[i];
         }else{
            act_func[i]=act_func_5[i];
         }
         for (int j=0;j<MAX_NEURON;j++){
              for (int k=0;k<MAX_NEURON;k++){
                  if (network_no==1) {
                     network[i][j][k]=network_1[i][j][k];
                  }else if (network_no==2) {
                     network[i][j][k]=network_2[i][j][k];
                  }else if (network_no==3) {
                     network[i][j][k]=network_3[i][j][k];
                  }else if (network_no==4) {
                     network[i][j][k]=network_4[i][j][k];
                  }else{
                     network[i][j][k]=network_5[i][j][k];
                  }
              }
         }
    }

   double inputs[];
   ArrayResize(inputs, ArraySize(original_inputs));
   ArrayCopy(inputs,original_inputs);

   for(int i=0;i<ArraySize(act_func);i++){
      double new_inputs[];
      int j=0;
      while (network[i][j][0]!=EMPTY_VALUE){
         //Print("j= ",j);
         int no_weight=  ArraySize(inputs);
         double sum=network[i][j][no_weight];
         for (int k=0;k<no_weight;k++){
            sum+=inputs[k]*network[i][j][k];
         }
         ArrayResize(new_inputs,j+1);
         new_inputs[j]=sum;
         j+=1;
      }
      activation(new_inputs,act_func[i]);
      ArrayInitialize(inputs,0);
      ArrayResize(inputs, ArraySize(new_inputs));
      ArrayCopy(inputs,new_inputs);
   }
   ArrayResize(result,ArraySize(inputs));
   ArrayCopy(result,inputs);
}

void activation(double &oSums[], string func) {

   if (func=="tanh"){
      for (int i=0;i<ArraySize(oSums);i++){
            oSums[i]=tanh(oSums[i]);
      }

   }else if (func=="relu"){
      for (int i=0;i<ArraySize(oSums);i++){
         if (oSums[i]<=0){
            oSums[i]=0;
         }
      }
   }else if (func=="softsign"){
      for (int i=0;i<ArraySize(oSums);i++){
         oSums[i] = oSums[i]/(MathAbs(oSums[i])+1);
      }
   }else if (func=="sigmoid"){
      for (int i=0;i<ArraySize(oSums);i++){
         oSums[i] = 1/(1+exp(-oSums[i]));
      }
   }else if (func=="softmax"){
      // determine max output sum
      // does all output nodes at once so scale doesn't have to be re-computed each time
      int size=ArraySize(oSums);
      double max= oSums[0];
      for(int i = 0; i<size;++i)
         if(oSums[i]>max) max=oSums[i];

      // determine scaling factor -- sum of exp(each val - max)
      double scale=0.0;
      for(int i= 0; i<size;++i){
         scale+= MathExp(oSums[i]-max);
      }
      for(int i=0; i<size;++i)
         oSums[i]=MathExp(oSums[i]-max)/scale;
   }
}

void load_network(string file_name, int network_no){

   string act_func [];
   double network [][MAX_NEURON][MAX_NEURON];

   int handle = FileOpen(file_name,FILE_READ|FILE_TXT|FILE_ANSI|FILE_COMMON);
   if(handle!=INVALID_HANDLE){
      double layer [MAX_NEURON][MAX_NEURON];
      int neuron_count=0;
      int layer_count=0;
      while(!FileIsEnding(handle)) {
         string line=FileReadString(handle);
         if (StringFind(line,"#")<0){
            StringReplace(line,"[","");
            StringReplace(line,"]","");
            string result[];
            StringSplit(line,44,result);
            for (int i=0;i<ArraySize(result);i++){
               layer[neuron_count][i]=StringToDouble(result[i]);
            }
            for (int j=ArraySize(result);j<MAX_NEURON;j++){
               layer[neuron_count][j]=EMPTY_VALUE;
            }
            neuron_count+=1;
         }else{
            StringReplace(line,"#","");
            layer_count+=1;
            ArrayResize(act_func,layer_count);
            act_func[layer_count-1]=line;
            for (int i=neuron_count;i<MAX_NEURON;i++){
               for (int j=0;j<MAX_NEURON;j++){
                  layer[i][j]=EMPTY_VALUE;
               }
            }
            neuron_count=0;
            ArrayResize(network,layer_count);
            for (int i=neuron_count;i<MAX_NEURON;i++){
               for (int j=0;j<MAX_NEURON;j++){
                  network[layer_count-1][i][j]=layer[i][j];
               }
            }
         }
      }
      FileClose(handle);


      if (network_no==1) {
         ArrayResize(act_func_1,ArraySize(act_func));
         ArrayResize(network_1,ArraySize(act_func));
      }else if (network_no==2){
         ArrayResize(act_func_2,ArraySize(act_func));
         ArrayResize(network_2,ArraySize(act_func));
      }else if (network_no==3){
         ArrayResize(act_func_3,ArraySize(act_func));
         ArrayResize(network_3,ArraySize(act_func));
      }else if (network_no==4){
         ArrayResize(act_func_4,ArraySize(act_func));
         ArrayResize(network_4,ArraySize(act_func));
      }else{
         ArrayResize(act_func_5,ArraySize(act_func));
         ArrayResize(network_5,ArraySize(act_func));
      }

      for (int i=0;i<ArraySize(act_func);i++){
            if (network_no==1) {
               act_func_1[i]=act_func[i];
            }else if (network_no==2) {
               act_func_2[i]=act_func[i];
            }else if (network_no==3) {
               act_func_3[i]=act_func[i];
            }else if (network_no==4) {
               act_func_4[i]=act_func[i];
            }else {
               act_func_5[i]=act_func[i];
            }
            for (int j=0;j<MAX_NEURON;j++){
                 for (int k=0;k<MAX_NEURON;k++){
                     if (network_no==1) {
                        network_1[i][j][k]=network[i][j][k];
                     }else if (network_no==2) {
                        network_2[i][j][k]=network[i][j][k];
                     }else if (network_no==3) {
                        network_3[i][j][k]=network[i][j][k];
                     }else if (network_no==4) {
                        network_4[i][j][k]=network[i][j][k];
                     }else {
                        network_5[i][j][k]=network[i][j][k];
                     }
                 }
            }
       }
   }

}

int GetMinute()
  {
   switch(Period())
     {
      case PERIOD_M1: return(1);
      case PERIOD_M2: return(2);
      case PERIOD_M3: return(3);
      case PERIOD_M4: return(4);
      case PERIOD_M5: return(5);
      case PERIOD_M6: return(6);
      case PERIOD_M10: return(10);
      case PERIOD_M12: return(12);
      case PERIOD_M15: return(15);
      case PERIOD_M20: return(20);
      case PERIOD_M30: return(30);
      case PERIOD_H1: return(60);
      case PERIOD_H2: return(120);
      case PERIOD_H3: return(180);
      case PERIOD_H4: return(240);
      case PERIOD_H6: return(360);
      case PERIOD_H8: return(480);
      case PERIOD_H12: return(720);
      case PERIOD_D1: return(1440);
      case PERIOD_W1: return(10080);
      case PERIOD_MN1: return(43200);
     }
   // by default it will return 1 minute
   return(1);
  }


int check_condition(double &open_candles[],double &close_candles[],double &high_candles[],double &low_candles[]){
   double diff_buf[5];
   for (int i=0;i<ArraySize(diff_buf);i++){
      if (high_candles[i]-low_candles[i]==0) {
         diff_buf[i]=0;
      }else {
         diff_buf[i]=(close_candles[i]-open_candles[i])*MathSqrt(MathAbs(close_candles[i]-open_candles[i])/(high_candles[i]-low_candles[i]));
      }
   }
   double first_sum=ArraySum(diff_buf);
   double cvp=cvp_calculator(diff_buf);
   if (cvp<=100 && first_sum!=0 ){
      if (first_sum >0 ){
         return 1;
      }else{
         return 0;
      }
   }
   return -1;
}


int ArrayCount(int &arr[], int count_val){
   int count=0;
   for (int i=0;i< ArraySize(arr);i++){
      if (arr[i]==count_val){
         count++;
      }
   }
   return count;
}


double cvp_calculator(double &array[]){
   double sum=ArraySum(array);
   double average=sum/ArraySize(array);
   if (average==0){
      return 200;
   }
   double sum_square=0;
   for (int i=0;i<ArraySize(array);i++){
      sum_square+=MathPow(array[i]-average,2.0);
   }
   double cv=MathPow(sum_square/ArraySize(array),0.5);
   double R =MathAbs(cv/average*100);
   if (R>100){
      R=200;
   }
   return R;
}

double ArraySum(double &array[]){
   double sum=0;
   for (int i=0;i<ArraySize(array);i++){
      sum+=array[i];
   }
   return sum;
}

void CloseAllPositions()
  {
   for(int i=PositionsTotal()-1;i>=0;i--) // returns the number of current positions
      if(m_position.SelectByIndex(i))     // selects the position by index for further access to its properties
         if(m_position.Symbol()==Symbol() && m_position.Magic()==EXPERT_MAGIC)
            m_trade.PositionClose(m_position.Ticket()); // close a position by the specified symbol
  }
//+------------------------------------------------------------------+
//|                                                  data_writer.mq5 |
//|                                  Copyright 2021, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"

#define AMOUNT 8;
#define WRITE_DELAY 2;
#define TIMEFRAME_1 14;
#define TIMEFRAME_2 24;

int amount=AMOUNT;
int timeframe1= TIMEFRAME_1; int timeframe2=TIMEFRAME_2;

int            iRSI_handle;                              
double         iRSI_buf[];


int            iRSI_handle_m2_tf1, iRSI_handle_m2_tf2, iRSI_handle_m3_tf1, iRSI_handle_m3_tf2, iRSI_handle_m5_tf1, iRSI_handle_m5_tf2, iRSI_handle_m10_tf1, iRSI_handle_m10_tf2;                             
double         iRSI_buf_m2_tf1[], iRSI_buf_m2_tf2[], iRSI_buf_m3_tf1[], iRSI_buf_m3_tf2[], iRSI_buf_m5_tf1[], iRSI_buf_m5_tf2[], iRSI_buf_m10_tf1[], iRSI_buf_m10_tf2[];

int            iMFI_handle_m2_tf1, iMFI_handle_m2_tf2, iMFI_handle_m3_tf1, iMFI_handle_m3_tf2, iMFI_handle_m5_tf1, iMFI_handle_m5_tf2, iMFI_handle_m10_tf1, iMFI_handle_m10_tf2;                             
double         iMFI_buf_m2_tf1[], iMFI_buf_m2_tf2[], iMFI_buf_m3_tf1[], iMFI_buf_m3_tf2[], iMFI_buf_m5_tf1[], iMFI_buf_m5_tf2[], iMFI_buf_m10_tf1[], iMFI_buf_m10_tf2[];

int            iStochastic_handle_m2,iStochastic_handle_m3,iStochastic_handle_m5,iStochastic_handle_m10;  
double         StochasticBuffer_m2[],StochasticBuffer_m3[],StochasticBuffer_m5[],StochasticBuffer_m10[];
double         StochasticSignalBuffer_m2[],StochasticSignalBuffer_m3[],StochasticSignalBuffer_m5[],StochasticSignalBuffer_m10[];

int            iStochastic_handle_m2_tf2,iStochastic_handle_m3_tf2,iStochastic_handle_m5_tf2,iStochastic_handle_m10_tf2;  
double         StochasticBuffer_m2_tf2[],StochasticBuffer_m3_tf2[],StochasticBuffer_m5_tf2[],StochasticBuffer_m10_tf2[];
double         StochasticSignalBuffer_m2_tf2[],StochasticSignalBuffer_m3_tf2[],StochasticSignalBuffer_m5_tf2[],StochasticSignalBuffer_m10_tf2[];

int            iUO_handle_m2_tf1, iUO_handle_m2_tf2, iUO_handle_m3_tf1, iUO_handle_m3_tf2, iUO_handle_m5_tf1, iUO_handle_m5_tf2, iUO_handle_m10_tf1, iUO_handle_m10_tf2;                             
double         iUO_buf_m2_tf1[], iUO_buf_m2_tf2[], iUO_buf_m3_tf1[], iUO_buf_m3_tf2[], iUO_buf_m5_tf1[], iUO_buf_m5_tf2[], iUO_buf_m10_tf1[], iUO_buf_m10_tf2[];

int            iWPR_handle_m2_tf1, iWPR_handle_m2_tf2, iWPR_handle_m3_tf1, iWPR_handle_m3_tf2, iWPR_handle_m5_tf1, iWPR_handle_m5_tf2, iWPR_handle_m10_tf1, iWPR_handle_m10_tf2;                             
double         iWPR_buf_m2_tf1[], iWPR_buf_m2_tf2[], iWPR_buf_m3_tf1[], iWPR_buf_m3_tf2[], iWPR_buf_m5_tf1[], iWPR_buf_m5_tf2[], iWPR_buf_m10_tf1[], iWPR_buf_m10_tf2[];

int            iGolden_handle;   
double         iGolden_buf[];

bool shutdown = false;

ENUM_APPLIED_PRICE   applied_price=PRICE_TYPICAL;  
ENUM_APPLIED_VOLUME  applied_volume=VOLUME_TICK;
 
double            High_buf[];  
double            Low_buf[];
double            Open_buf[];  
double            Close_buf[];

int file_1_handler=FileOpen("RSI_data.csv",FILE_WRITE|FILE_ANSI|FILE_CSV,",");
int file_2_handler=FileOpen("Stoch_data.csv",FILE_WRITE|FILE_ANSI|FILE_CSV,",");
int file_3_handler=FileOpen("MFI_data.csv",FILE_WRITE|FILE_ANSI|FILE_CSV,",");
int file_4_handler=FileOpen("UO_data.csv",FILE_WRITE|FILE_ANSI|FILE_CSV,",");
int file_5_handler=FileOpen("WPR_data.csv",FILE_WRITE|FILE_ANSI|FILE_CSV,",");

int write_delay=WRITE_DELAY;
datetime pausetime=TimeCurrent();
datetime m_prev_bars                = 0;   
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+

int OnInit()
  {
  if(file_1_handler==INVALID_HANDLE){
      Alert("Error opening file");
      FileClose(file_1_handler);
   }
   if(file_2_handler==INVALID_HANDLE){
      Alert("Error opening file");
      FileClose(file_2_handler);
   }  
   if(file_3_handler==INVALID_HANDLE){
      Alert("Error opening file");
      FileClose(file_3_handler);
   }
   if(file_4_handler==INVALID_HANDLE){
      Alert("Error opening file");
      FileClose(file_4_handler);
   }
   if(file_5_handler==INVALID_HANDLE){
      Alert("Error opening file");
      FileClose(file_5_handler);
   }          
//--- create timer
   iRSI_handle=iRSI(Symbol(),PERIOD_CURRENT,14,applied_price);

   
   iRSI_handle_m2_tf1=iRSI(Symbol(),PERIOD_M2,timeframe1,applied_price);
   iRSI_handle_m3_tf1=iRSI(Symbol(),PERIOD_M3,timeframe1,applied_price);
   iRSI_handle_m5_tf1=iRSI(Symbol(),PERIOD_M5,timeframe1,applied_price);
   iRSI_handle_m10_tf1=iRSI(Symbol(),PERIOD_M10,timeframe1,applied_price);
   
   iRSI_handle_m2_tf2=iRSI(Symbol(),PERIOD_M2,timeframe2,applied_price);
   iRSI_handle_m3_tf2=iRSI(Symbol(),PERIOD_M3,timeframe2,applied_price);
   iRSI_handle_m5_tf2=iRSI(Symbol(),PERIOD_M5,timeframe2,applied_price);
   iRSI_handle_m10_tf2=iRSI(Symbol(),PERIOD_M10,timeframe2,applied_price);
   
   iStochastic_handle_m2=iStochastic(Symbol(),PERIOD_M2,5,3,3,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m3=iStochastic(Symbol(),PERIOD_M3,5,3,3,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m5=iStochastic(Symbol(),PERIOD_M5,5,3,3,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m10=iStochastic(Symbol(),PERIOD_M10,5,3,3,MODE_SMA,STO_LOWHIGH); 
   
   iStochastic_handle_m2_tf2=iStochastic(Symbol(),PERIOD_M2,13,8,8,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m3_tf2=iStochastic(Symbol(),PERIOD_M3,13,8,8,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m5_tf2=iStochastic(Symbol(),PERIOD_M5,13,8,8,MODE_SMA,STO_LOWHIGH);
   iStochastic_handle_m10_tf2=iStochastic(Symbol(),PERIOD_M10,13,8,8,MODE_SMA,STO_LOWHIGH);      
   
      
   iUO_handle_m2_tf1=iCustom(Symbol(),PERIOD_M2,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   iUO_handle_m3_tf1=iCustom(Symbol(),PERIOD_M3,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   iUO_handle_m5_tf1=iCustom(Symbol(),PERIOD_M5,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   iUO_handle_m10_tf1=iCustom(Symbol(),PERIOD_M10,"Examples\\Ultimate_Oscillator",7,14,28,4,2,1);
   
   iUO_handle_m2_tf2=iCustom(Symbol(),PERIOD_M2,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   iUO_handle_m3_tf2=iCustom(Symbol(),PERIOD_M3,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   iUO_handle_m5_tf2=iCustom(Symbol(),PERIOD_M5,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   iUO_handle_m10_tf2=iCustom(Symbol(),PERIOD_M10,"Examples\\Ultimate_Oscillator",8,17,33,5,3,2);
   
   iMFI_handle_m2_tf1=iMFI(Symbol(),PERIOD_M2,timeframe1,applied_volume);
   iMFI_handle_m3_tf1=iMFI(Symbol(),PERIOD_M3,timeframe1,applied_volume);
   iMFI_handle_m5_tf1=iMFI(Symbol(),PERIOD_M5,timeframe1,applied_volume);
   iMFI_handle_m10_tf1=iMFI(Symbol(),PERIOD_M10,timeframe1,applied_volume);
   
   iMFI_handle_m2_tf2=iMFI(Symbol(),PERIOD_M2,timeframe2,applied_volume);
   iMFI_handle_m3_tf2=iMFI(Symbol(),PERIOD_M3,timeframe2,applied_volume);
   iMFI_handle_m5_tf2=iMFI(Symbol(),PERIOD_M5,timeframe2,applied_volume);
   iMFI_handle_m10_tf2=iMFI(Symbol(),PERIOD_M10,timeframe2,applied_volume);   
   
   
   //iWPR_handle_m2_tf1=iCustom(Symbol(),PERIOD_M2,"Examples\\WPR",timeframe1);
   //iWPR_handle_m3_tf1=iCustom(Symbol(),PERIOD_M3,"Examples\\WPR",timeframe1);
   //iWPR_handle_m5_tf1=iCustom(Symbol(),PERIOD_M5,"Examples\\WPR",timeframe1);
   //iWPR_handle_m10_tf1=iCustom(Symbol(),PERIOD_M10,"Examples\\WPR",timeframe1);
   
   //iWPR_handle_m2_tf2=iCustom(Symbol(),PERIOD_M2,"Examples\\WPR",timeframe2);
   //iWPR_handle_m3_tf2=iCustom(Symbol(),PERIOD_M3,"Examples\\WPR",timeframe2);
   //iWPR_handle_m5_tf2=iCustom(Symbol(),PERIOD_M5,"Examples\\WPR",timeframe2);
   //iWPR_handle_m10_tf2=iCustom(Symbol(),PERIOD_M10,"Examples\\WPR",timeframe2); 
   
   //iGolden_handle=iCustom(Symbol(),PERIOD_M5,"Custom\\Golden_Ratio",timeframe1);
   
   iWPR_handle_m2_tf1=iCustom(Symbol(),PERIOD_M2,"Custom\\Golden_Ratio",timeframe1);
   iWPR_handle_m3_tf1=iCustom(Symbol(),PERIOD_M3,"Custom\\Golden_Ratio",timeframe1);
   iWPR_handle_m5_tf1=iCustom(Symbol(),PERIOD_M10,"Custom\\Golden_Ratio",timeframe1);
   iWPR_handle_m10_tf1=iCustom(Symbol(),PERIOD_M10,"Custom\\Golden_Ratio",timeframe1);
   
   iWPR_handle_m2_tf2=iCustom(Symbol(),PERIOD_M2,"Custom\\Golden_Ratio",timeframe2);
   iWPR_handle_m3_tf2=iCustom(Symbol(),PERIOD_M3,"Custom\\Golden_Ratio",timeframe2);
   iWPR_handle_m5_tf2=iCustom(Symbol(),PERIOD_M5,"Custom\\Golden_Ratio",timeframe2);
   iWPR_handle_m10_tf2=iCustom(Symbol(),PERIOD_M10,"Custom\\Golden_Ratio",timeframe2); 
   
   //iMFI_handle_m2_tf1=iCCI(Symbol(),PERIOD_CURRENT,timeframe1,applied_price);
   //iMFI_handle_m3_tf1=iCCI(Symbol(),PERIOD_CURRENT,timeframe1,applied_price);
   //iMFI_handle_m5_tf1=iCCI(Symbol(),PERIOD_CURRENT,timeframe1,applied_price);
   //iMFI_handle_m10_tf1=iCCI(Symbol(),PERIOD_CURRENT,timeframe1,applied_price);
   
   //iMFI_handle_m2_tf2=iCCI(Symbol(),PERIOD_CURRENT,timeframe2,applied_price);
   //iMFI_handle_m3_tf2=iCCI(Symbol(),PERIOD_CURRENT,timeframe2,applied_price);
   //iMFI_handle_m5_tf2=iCCI(Symbol(),PERIOD_CURRENT,timeframe2,applied_price);
   //iMFI_handle_m10_tf2=iCCI(Symbol(),PERIOD_CURRENT,timeframe2,applied_price);
   

   ArraySetAsSeries(iRSI_buf,true);
  
      
   ArraySetAsSeries(iRSI_buf_m2_tf1,true); 
   ArraySetAsSeries(iRSI_buf_m3_tf1,true);
   ArraySetAsSeries(iRSI_buf_m5_tf1,true);   
   ArraySetAsSeries(iRSI_buf_m10_tf1,true); 
   
   ArraySetAsSeries(iRSI_buf_m2_tf2,true); 
   ArraySetAsSeries(iRSI_buf_m3_tf2,true);
   ArraySetAsSeries(iRSI_buf_m5_tf2,true);   
   ArraySetAsSeries(iRSI_buf_m10_tf2,true); 
      
   
   ArraySetAsSeries(StochasticBuffer_m2,true);
   ArraySetAsSeries(StochasticBuffer_m3,true);
   ArraySetAsSeries(StochasticBuffer_m5,true);
   ArraySetAsSeries(StochasticBuffer_m10,true);
   
   ArraySetAsSeries(StochasticBuffer_m2_tf2,true);
   ArraySetAsSeries(StochasticBuffer_m3_tf2,true);
   ArraySetAsSeries(StochasticBuffer_m5_tf2,true);
   ArraySetAsSeries(StochasticBuffer_m10_tf2,true);
   
   ArraySetAsSeries(StochasticSignalBuffer_m2,true);            
   ArraySetAsSeries(StochasticSignalBuffer_m3,true);
   ArraySetAsSeries(StochasticSignalBuffer_m5,true);
   ArraySetAsSeries(StochasticSignalBuffer_m10,true); 
   
   ArraySetAsSeries(StochasticSignalBuffer_m2_tf2,true);            
   ArraySetAsSeries(StochasticSignalBuffer_m3_tf2,true);
   ArraySetAsSeries(StochasticSignalBuffer_m5_tf2,true);
   ArraySetAsSeries(StochasticSignalBuffer_m10_tf2,true); 
    
   ArraySetAsSeries(iMFI_buf_m2_tf1,true); 
   ArraySetAsSeries(iMFI_buf_m3_tf1,true);
   ArraySetAsSeries(iMFI_buf_m5_tf1,true);   
   ArraySetAsSeries(iMFI_buf_m10_tf1,true); 
   
   ArraySetAsSeries(iMFI_buf_m2_tf2,true); 
   ArraySetAsSeries(iMFI_buf_m3_tf2,true);
   ArraySetAsSeries(iMFI_buf_m5_tf2,true);   
   ArraySetAsSeries(iMFI_buf_m10_tf2,true);  
    
                              
   ArraySetAsSeries(High_buf,true);
   ArraySetAsSeries(Low_buf,true);
   
   double t_arr[]={6,-6,7,9,-3};
   Print("test: ",cvp_calculator(t_arr));
   //shutdown=true;
//---
   Print("Get minute: ", GetMinute()*60);
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {

   ArrayFree(iRSI_buf);

   
   ArrayFree(iRSI_buf_m2_tf1);
   ArrayFree(iRSI_buf_m3_tf1);
   ArrayFree(iRSI_buf_m5_tf1);   
   ArrayFree(iRSI_buf_m10_tf1);
   
   ArrayFree(iRSI_buf_m2_tf2);
   ArrayFree(iRSI_buf_m3_tf2);
   ArrayFree(iRSI_buf_m5_tf2);   
   ArrayFree(iRSI_buf_m10_tf2);
    
   ArrayFree(StochasticBuffer_m2);
   ArrayFree(StochasticBuffer_m3);
   ArrayFree(StochasticBuffer_m5);
   ArrayFree(StochasticBuffer_m10);
   
   ArrayFree(StochasticSignalBuffer_m2);            
   ArrayFree(StochasticSignalBuffer_m3);
   ArrayFree(StochasticSignalBuffer_m5);
   ArrayFree(StochasticSignalBuffer_m10);  
   
   ArrayFree(iMFI_buf_m2_tf1);
   ArrayFree(iMFI_buf_m3_tf1);
   ArrayFree(iMFI_buf_m5_tf1);   
   ArrayFree(iMFI_buf_m10_tf1);
   
   ArrayFree(iMFI_buf_m2_tf2);
   ArrayFree(iMFI_buf_m3_tf2);
   ArrayFree(iMFI_buf_m5_tf2);   
   ArrayFree(iMFI_buf_m10_tf2);
     

   
   ArrayFree(High_buf);                                    
   ArrayFree(Low_buf);
   FileClose(file_1_handler);
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   if (shutdown){
      return;
   }
   datetime time_0=iTime(Symbol(),Period(),0);
   if(time_0==m_prev_bars)
      return;
   m_prev_bars=time_0;
   
   int err1=CopyBuffer(iGolden_handle,0,1,amount,iGolden_buf);

   
   err1=CopyBuffer(iRSI_handle_m2_tf1,0,1,amount,iRSI_buf_m2_tf1);
   err1=CopyBuffer(iRSI_handle_m3_tf1,0,1,amount,iRSI_buf_m3_tf1);
   err1=CopyBuffer(iRSI_handle_m5_tf1,0,1,amount,iRSI_buf_m5_tf1);   
   err1=CopyBuffer(iRSI_handle_m10_tf1,0,1,amount,iRSI_buf_m10_tf1);
   
   err1=CopyBuffer(iRSI_handle_m2_tf2,0,1,amount,iRSI_buf_m2_tf2);
   err1=CopyBuffer(iRSI_handle_m3_tf2,0,1,amount,iRSI_buf_m3_tf2);
   err1=CopyBuffer(iRSI_handle_m5_tf2,0,1,amount,iRSI_buf_m5_tf2);   
   err1=CopyBuffer(iRSI_handle_m10_tf2,0,1,amount,iRSI_buf_m10_tf2);
   
   err1=CopyBuffer(iStochastic_handle_m2,0,1,amount,StochasticBuffer_m2);
   err1=CopyBuffer(iStochastic_handle_m3,0,1,amount,StochasticBuffer_m3);
   err1=CopyBuffer(iStochastic_handle_m5,0,1,amount,StochasticBuffer_m5); 
   err1=CopyBuffer(iStochastic_handle_m10,0,1,amount,StochasticBuffer_m10);
        
   err1=CopyBuffer(iStochastic_handle_m2,1,1,amount,StochasticSignalBuffer_m2);
   err1=CopyBuffer(iStochastic_handle_m3,1,1,amount,StochasticSignalBuffer_m3);
   err1=CopyBuffer(iStochastic_handle_m5,1,1,amount,StochasticSignalBuffer_m5);   
   err1=CopyBuffer(iStochastic_handle_m10,1,1,amount,StochasticSignalBuffer_m10);
   
   err1=CopyBuffer(iStochastic_handle_m2_tf2,0,1,amount,StochasticBuffer_m2_tf2);
   err1=CopyBuffer(iStochastic_handle_m3_tf2,0,1,amount,StochasticBuffer_m3_tf2);
   err1=CopyBuffer(iStochastic_handle_m5_tf2,0,1,amount,StochasticBuffer_m5_tf2); 
   err1=CopyBuffer(iStochastic_handle_m10_tf2,0,1,amount,StochasticBuffer_m10_tf2);
        
   err1=CopyBuffer(iStochastic_handle_m2_tf2,1,1,amount,StochasticSignalBuffer_m2_tf2);
   err1=CopyBuffer(iStochastic_handle_m3_tf2,1,1,amount,StochasticSignalBuffer_m3_tf2);
   err1=CopyBuffer(iStochastic_handle_m5_tf2,1,1,amount,StochasticSignalBuffer_m5_tf2);   
   err1=CopyBuffer(iStochastic_handle_m10_tf2,1,1,amount,StochasticSignalBuffer_m10_tf2);
   
   err1=CopyBuffer(iMFI_handle_m2_tf1,0,1,amount,iMFI_buf_m2_tf1);
   err1=CopyBuffer(iMFI_handle_m3_tf1,0,1,amount,iMFI_buf_m3_tf1);
   err1=CopyBuffer(iMFI_handle_m5_tf1,0,1,amount,iMFI_buf_m5_tf1);   
   err1=CopyBuffer(iMFI_handle_m10_tf1,0,1,amount,iMFI_buf_m10_tf1);
   
   err1=CopyBuffer(iMFI_handle_m2_tf2,0,1,amount,iMFI_buf_m2_tf2);
   err1=CopyBuffer(iMFI_handle_m3_tf2,0,1,amount,iMFI_buf_m3_tf2);
   err1=CopyBuffer(iMFI_handle_m5_tf2,0,1,amount,iMFI_buf_m5_tf2);   
   err1=CopyBuffer(iMFI_handle_m10_tf2,0,1,amount,iMFI_buf_m10_tf2);
   
   err1=CopyBuffer(iUO_handle_m2_tf1,0,1,amount,iUO_buf_m2_tf1);
   err1=CopyBuffer(iUO_handle_m3_tf1,0,1,amount,iUO_buf_m3_tf1);
   err1=CopyBuffer(iUO_handle_m5_tf1,0,1,amount,iUO_buf_m5_tf1);   
   err1=CopyBuffer(iUO_handle_m10_tf1,0,1,amount,iUO_buf_m10_tf1);
   
   err1=CopyBuffer(iUO_handle_m2_tf2,0,1,amount,iUO_buf_m2_tf2);
   err1=CopyBuffer(iUO_handle_m3_tf2,0,1,amount,iUO_buf_m3_tf2);
   err1=CopyBuffer(iUO_handle_m5_tf2,0,1,amount,iUO_buf_m5_tf2);   
   err1=CopyBuffer(iUO_handle_m10_tf2,0,1,amount,iUO_buf_m10_tf2);
   
   err1=CopyBuffer(iWPR_handle_m2_tf1,0,1,amount,iWPR_buf_m2_tf1);
   err1=CopyBuffer(iWPR_handle_m3_tf1,0,1,amount,iWPR_buf_m3_tf1);
   err1=CopyBuffer(iWPR_handle_m5_tf1,0,1,amount,iWPR_buf_m5_tf1);   
   err1=CopyBuffer(iWPR_handle_m10_tf1,0,1,amount,iWPR_buf_m10_tf1);
   
   err1=CopyBuffer(iWPR_handle_m2_tf2,0,1,amount,iWPR_buf_m2_tf2);
   err1=CopyBuffer(iWPR_handle_m3_tf2,0,1,amount,iWPR_buf_m3_tf2);
   err1=CopyBuffer(iWPR_handle_m5_tf2,0,1,amount,iWPR_buf_m5_tf2);   
   err1=CopyBuffer(iWPR_handle_m10_tf2,0,1,amount,iWPR_buf_m10_tf2);
      
  

         
   int err2=CopyHigh(Symbol(),PERIOD_CURRENT,1,amount,High_buf);  
   int err3=CopyLow(Symbol(),PERIOD_CURRENT,1,amount,Low_buf);
  // int r=check_candles(High_buf,Low_buf); 
   err2=CopyOpen(Symbol(),PERIOD_CURRENT,1,amount,Open_buf);  
   err3=CopyClose(Symbol(),PERIOD_CURRENT,1,amount,Close_buf);
   int r=check_candles_2(Open_buf,Close_buf,High_buf,Low_buf); 


   if (r>=0&&file_1_handler>=0&&file_2_handler>=0&&file_3_handler>=0){
      if (pausetime<=TimeCurrent()){
            FileWrite(file_1_handler, 
            DoubleToString(iRSI_buf_m2_tf1[amount-1]),   
            DoubleToString(iRSI_buf_m3_tf1[amount-1]),   
            DoubleToString(iRSI_buf_m5_tf1[amount-1]),   
            DoubleToString(iRSI_buf_m10_tf1[amount-1]), 
   
            DoubleToString(iRSI_buf_m2_tf2[amount-1]),   
            DoubleToString(iRSI_buf_m3_tf2[amount-1]),   
            DoubleToString(iRSI_buf_m5_tf2[amount-1]),   
            DoubleToString(iRSI_buf_m10_tf2[amount-1]),

            DoubleToString(StochasticBuffer_m2[amount-1]),
            DoubleToString(StochasticBuffer_m3[amount-1]),
            DoubleToString(StochasticBuffer_m5[amount-1]),
            DoubleToString(StochasticBuffer_m10[amount-1]),

            DoubleToString(StochasticSignalBuffer_m2[amount-1]),
            DoubleToString(StochasticSignalBuffer_m3[amount-1]),
            DoubleToString(StochasticSignalBuffer_m5[amount-1]),
            DoubleToString(StochasticSignalBuffer_m10[amount-1]),

            DoubleToString(StochasticBuffer_m2_tf2[amount-1]),
            DoubleToString(StochasticBuffer_m3_tf2[amount-1]),
            DoubleToString(StochasticBuffer_m5_tf2[amount-1]),
            DoubleToString(StochasticBuffer_m10_tf2[amount-1]),

            DoubleToString(StochasticSignalBuffer_m2_tf2[amount-1]),
            DoubleToString(StochasticSignalBuffer_m3_tf2[amount-1]),
            DoubleToString(StochasticSignalBuffer_m5_tf2[amount-1]),
            DoubleToString(StochasticSignalBuffer_m10_tf2[amount-1]),
            
//            DoubleToString(iUO_buf_m2_tf1[amount-1]),
//            DoubleToString(iUO_buf_m3_tf1[amount-1]),
//            DoubleToString(iUO_buf_m5_tf1[amount-1]),
//            DoubleToString(iUO_buf_m10_tf1[amount-1]),
//
//            DoubleToString(iUO_buf_m2_tf2[amount-1]),
//            DoubleToString(iUO_buf_m3_tf2[amount-1]),
//            DoubleToString(iUO_buf_m5_tf2[amount-1]),
//            DoubleToString(iUO_buf_m10_tf2[amount-1]),
                     
            IntegerToString(r)); 
                     
            FileWrite(file_2_handler,              
            DoubleToString(StochasticBuffer_m2[amount-1]),
            DoubleToString(StochasticBuffer_m3[amount-1]),
            DoubleToString(StochasticBuffer_m5[amount-1]),
            DoubleToString(StochasticBuffer_m10[amount-1]),
            
            DoubleToString(StochasticSignalBuffer_m2[amount-1]),            
            DoubleToString(StochasticSignalBuffer_m3[amount-1]),            
            DoubleToString(StochasticSignalBuffer_m5[amount-1]),            
            DoubleToString(StochasticSignalBuffer_m10[amount-1]),   
            
            DoubleToString(StochasticBuffer_m2_tf2[amount-1]),
            DoubleToString(StochasticBuffer_m3_tf2[amount-1]),
            DoubleToString(StochasticBuffer_m5_tf2[amount-1]),
            DoubleToString(StochasticBuffer_m10_tf2[amount-1]),
            
            DoubleToString(StochasticSignalBuffer_m2_tf2[amount-1]),            
            DoubleToString(StochasticSignalBuffer_m3_tf2[amount-1]),            
            DoubleToString(StochasticSignalBuffer_m5_tf2[amount-1]),            
            DoubleToString(StochasticSignalBuffer_m10_tf2[amount-1]),   
                     
            IntegerToString(r));
            
            FileWrite(file_3_handler,
            DoubleToString(iMFI_buf_m2_tf1[amount-1]),   
            DoubleToString(iMFI_buf_m3_tf1[amount-1]),   
            DoubleToString(iMFI_buf_m5_tf1[amount-1]),   
            DoubleToString(iMFI_buf_m10_tf1[amount-1]), 
   
            DoubleToString(iMFI_buf_m2_tf2[amount-1]),   
            DoubleToString(iMFI_buf_m3_tf2[amount-1]),   
            DoubleToString(iMFI_buf_m5_tf2[amount-1]),   
            DoubleToString(iMFI_buf_m10_tf2[amount-1]),    
            
            DoubleToString(iWPR_buf_m2_tf1[amount-1]),   
            DoubleToString(iWPR_buf_m3_tf1[amount-1]),   
            DoubleToString(iWPR_buf_m5_tf1[amount-1]),   
            DoubleToString(iWPR_buf_m10_tf1[amount-1]), 
   
            DoubleToString(iWPR_buf_m2_tf2[amount-1]),   
            DoubleToString(iWPR_buf_m3_tf2[amount-1]),   
            DoubleToString(iWPR_buf_m5_tf2[amount-1]),   
            DoubleToString(iWPR_buf_m10_tf2[amount-1]),    
                     
            IntegerToString(r));           
            
            pausetime=TimeCurrent()+GetMinute()*60*5;    
         }        
      }
   }
   
  //}

int check_candles(double &high_candles[],double &low_candles[]){
   
   int amount_t=amount/2;
   double larger_count=-1,smaller_count=-1;
   if (low_candles[0]>low_candles[1]&&low_candles[0]>low_candles[2]&&low_candles[0]>low_candles[3]){ 
      smaller_count=0;    
      for (int i=0;i<amount_t;i++){
         if (low_candles[3]>low_candles[4+i]){
            smaller_count+=1;
         }
      }
   }
   if (high_candles[0]<high_candles[1]&&high_candles[0]<high_candles[2]&&high_candles[0]<high_candles[3]){    
      larger_count=0;  
      for (int i=0;i<amount_t;i++){
         if (high_candles[3]<high_candles[4+i]){
            larger_count+=1;
         }
      }
   }
   if (smaller_count==-1 && larger_count==-1) {
      return -1;
   }
   if (smaller_count==amount/2 && larger_count==amount/2) {
      return 2;
   }
   if (smaller_count==amount/2){
      return 0;
   }
   if (larger_count==amount/2){
      return 1;
   }   
   return 2;
}

int check_candles_2(double &open_candles[],double &close_candles[],double &high_candles[],double &low_candles[]){
   
   int amount_half=amount/2;
   double diff_buf[];
   ArrayResize(diff_buf,amount);
   //Print("Size_test: ", ArraySize(open_candles));
   for (int i=0;i<ArraySize(diff_buf);i++){
      if (high_candles[i]-low_candles[i]==0) {
         diff_buf[i]=0;
      }else {
         diff_buf[i]=(close_candles[i]-open_candles[i])*MathSqrt(MathAbs(close_candles[i]-open_candles[i])/(high_candles[i]-low_candles[i]));
      }   
   }
   double first_dif[], second_dif[];
   ArrayResize(first_dif,amount_half);
   ArrayResize(first_dif,amount-amount_half);
   ArrayCopy(first_dif,diff_buf,0,0,amount_half);
   double cvp=cvp_calculator(first_dif);
   ArrayCopy(second_dif,diff_buf,0,amount_half,amount-amount_half);
   double first_sum=ArraySum(first_dif);
   double second_sum=ArraySum(second_dif);
   if (cvp<=100 && first_sum!=0&& second_sum!=0 ){ 
      //if (MathAbs(first_sum)>MathAbs(second_sum)){
      //   return 2;
      //}
      if (MathAbs(first_sum)<MathAbs(second_sum)*1.618){    
         if( second_sum*first_sum>0 ){ //second_sum*first_sum>0 &&         
            //return 1;
            if(first_sum>0){
               return 1;//BUY
            }else {
              return 0;//SELL
            }        
         }else {  
            return 2;       
            //if(first_sum>0){
            //   return 0;//SELL
            //}else {
            //   return 1;//BUY
            //}              
         }
      }else {
         return 2;
      }             
   } 
   return -1;
}

double cvp_calculator(double &array[]){
   double sum=ArraySum(array);
   double average=sum/ArraySize(array);
   if (average==0){
      return 200;
   }
   double sum_square=0;
   for (int i=0;i<ArraySize(array);i++){
      sum_square+=MathPow(array[i]-average,2.0);
   }
   double cv=MathPow(sum_square/ArraySize(array),0.5);
   double R =MathAbs(cv/average*100);
   if (R>100){
      R=200;
   }
   return R;
}

double ArraySum(double &array[]){
   double sum=0;
   for (int i=0;i<ArraySize(array);i++){
      sum+=array[i];
   }
   return sum;
}

int GetMinute()
  {
   switch(Period())
     {
      case PERIOD_M1: return(1);
      case PERIOD_M2: return(2);
      case PERIOD_M3: return(3);
      case PERIOD_M4: return(4);
      case PERIOD_M5: return(5);
      case PERIOD_M6: return(6);
      case PERIOD_M10: return(10);
      case PERIOD_M12: return(12);
      case PERIOD_M15: return(15);
      case PERIOD_M20: return(20);
      case PERIOD_M30: return(30);
      case PERIOD_H1: return(60);
      case PERIOD_H2: return(120);
      case PERIOD_H3: return(180);
      case PERIOD_H4: return(240);
      case PERIOD_H6: return(360);
      case PERIOD_H8: return(480);
      case PERIOD_H12: return(720);
      case PERIOD_D1: return(1440);
      case PERIOD_W1: return(10080);
      case PERIOD_MN1: return(43200);
     }
   // by default it will return 1 minute
   return(1);
  }